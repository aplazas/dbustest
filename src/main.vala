/* main.vala
 *
 * Copyright 2018 Adrien Plazas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[DBus (name = "org.freedesktop.Flatpak.Development")]
private interface FlatpakDevelopment : Object {
//  <interface name='org.freedesktop.Flatpak.Development'>
//   <property name="version" type="u" access="read"/>

//   <!-- This methods let you start processes, sandboxed or not on the
//        host, so its not generally safe to allow access to it to any
//        application. However, at times it is very useful to be able
//        to do this. For instance for developer tools this lets you
//        build flatpaks from inside a flatpak. -->
//   <method name="HostCommand">
//     <arg type='ay' name='cwd_path' direction='in'/>
//     <arg type='aay' name='argv' direction='in'/>
//     <arg type='a{uh}' name='fds' direction='in'/>
//     <arg type='a{ss}' name='envs' direction='in'/>
//     <arg type='u' name='flags' direction='in'/>
//     <arg type='u' name='pid' direction='out'/>
//   </method>
// 	public abstract void host_command (string? cwd_path,
// 	                                   string[] argv,
// 	                                   uint32[]? fds,
// 	                                   string[]? envs,
// 	                                   uint32 flags,
// 	                                   out uint32 pid) throws DBusError, IOError;
	public abstract uint32 host_command (uint8[] cwd_path,
	                                     uint8[,] argv,
	                                     HashTable<uint32, uint32> fds,
	                                     HashTable<string, string> envs,
	                                     uint32 flags,
	                                     out uint32 pid) throws DBusError, IOError;
//   <method name="HostCommandSignal">
//     <arg type='u' name='pid' direction='in'/>
//     <arg type='u' name='signal' direction='in'/>
//     <arg type='b' name='to_process_group' direction='in'/>
//   </method>
//   <signal name="HostCommandExited">
//     <arg type='u' name='pid' direction='out'/>
//     <arg type='u' name='exit_status' direction='out'/>
//   </signal>

// </interface>

	// public signal void discovery_started ();
	// public signal void discovery_completed ();
	// public signal void remote_device_found (string address, uint klass, int rssi);
	// public signal void remote_name_updated (string address, string name);

	// public abstract void discover_devices () throws IOError;
}

void run_command () {
    FlatpakDevelopment flatpak_development;
    uint32 pid = 0;
    uint8[,] command = new uint8[2,6];
    command[0,0] = 'g';
    command[0,1] = 'e';
    command[0,2] = 'd';
    command[0,3] = 'i';
    command[0,4] = 't';
    command[0,5] = '\0';

    try {
        flatpak_development = Bus.get_proxy_sync (BusType.SESSION,
                                                  "org.freedesktop.Flatpak",
                                                  "/org/freedesktop/Flatpak/Development");

        flatpak_development.host_command ({ '\0' },
                                          command,
                                          new HashTable<uint32, uint> (direct_hash, direct_equal),
                                          new HashTable<string, string> (str_hash, str_equal),
                                          0,
                                          out pid);
    }
    catch (DBusError e) {
        stderr.printf ("Woops, DBusError: %s\n", e.message);
    }
    catch (IOError e) {
        stderr.printf ("Woops, IOError: %s\n", e.message);
    }
}

int main (string[] args) {
	var app = new Gtk.Application ("org.gnome.Dbustest", ApplicationFlags.FLAGS_NONE);
	app.activate.connect (() => {
		var win = app.active_window;
		if (win == null) {
			win = new Dbustest.Window (app);
		}
		win.present ();
		run_command ();
	});

	return app.run (args);
}
